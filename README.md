java-cambridge-tick0-part-1b
============================

Storage for tick0 of further java, a small challenge to attempt to create the fastest implementation of an external sort with limited memory.

http://www.cl.cam.ac.uk/teaching/1213/FJava/workbook0.html

http://www.cl.cam.ac.uk/teaching/1213/FJava/materials.html (Final leaderboard - the fastest entries gained a star :)
